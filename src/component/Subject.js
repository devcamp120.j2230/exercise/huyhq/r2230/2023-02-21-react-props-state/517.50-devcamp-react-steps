import { Component } from "react";

class Subject extends Component{
    render(){
        var subjectList = this.props.subjectProp;
        console.log(subjectList);
        return (
            <ul>
                {subjectList.map((value, index)=>{
                    return <li key={index}>{value.id}. {value.title}: {value.content}</li>
                })}
            </ul>
        );
    }
}

export default Subject;